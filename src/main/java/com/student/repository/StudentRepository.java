package com.student.repository;



import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.mongodb.repository.MongoRepository;
//import org.springframework.stereotype.Repository;

import com.student.entity.Student;

public interface StudentRepository extends JpaRepository<Student,Long> {
	
	//List<Student> findStudentDetailsByName(String name);

	//List<Student> findStudentDetailsByNameAndEmail(String name, String email);

}
