package com.student;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;
//import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
@Component
@EnableAutoConfiguration
@SpringBootApplication(exclude= {HibernateJpaAutoConfiguration.class})

//@EnableMongoRepositories("com.mongodb.repository")
//@ComponentScan("com.mongodbdemo.*")
public class StudentApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentApplication.class, args);
	}

}
