package com.student.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.student.entity.Address;
import com.student.entity.Student;
import com.student.service.StudentService;
import com.student.repository.AddressRepository;
//

//
//

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/student")
public class StudentController {
//	
	private static final Logger logger = LoggerFactory.getLogger(StudentController.class);


             @Autowired
             StudentService studentService;
             
             @Autowired
             AddressRepository addressRepository;

             @PostMapping("/create")
              public Student createStudent(@RequestBody Student student) {
            	   logger.info("Student data created");
            	   return studentService.createStudent(student);
	                
	                
	                }
             
             @PostMapping("/saveAddress")
             public Address saveAddress(@RequestBody Address address) {
           	   logger.info("Student data created");
           	   return addressRepository.save(address);
	                
	                
	                }
}
	                
//	           @GetMapping("/find/{id}" )
//               public Student findStudentDetails(@PathVariable String id) {
//            	   
//            	   return studentService.findStudentDetails(id);
//               }
//	           @GetMapping("/findbyName/{name}" )
//               public List<Student> findStudentDetailsByName(@PathVariable String name) {
//            	   
//            	   return studentService.findStudentDetailsByName(name);}
//            	   
//               @GetMapping("/findbyNameAndEmail" )
//               public List<Student> findStudentDetailsByNameAndEmail(@RequestParam String name,@RequestParam String email) {
//                return studentService.findStudentDetailsByNameAndEmail(name,email);
//            }
//	           
//	           
//	           
//                
//            
//	}
