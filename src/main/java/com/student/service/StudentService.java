package com.student.service;
  
  
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.stereotype.Service;
import com.student.entity.Student; 
import com.student.repository.StudentRepository;
  
@Service
public class StudentService {
  
  @Autowired 
  StudentRepository studentRepository;
  
  public Student createStudent(Student student) {
  
  return studentRepository.save(student);
  
  }}
/*
 * public Student findStudentDetails(String id) {
 * 
 * return studentRepository.findById(id).get(); }
 * 
 * //public List<Student> findStudentDetailsByName(String name) {
 * 
 * return studentRepository.findStudentDetailsByName(name); }
 * 
 * public List<Student> findStudentDetailsByNameAndEmail(String name,String
 * email) { return
 * studentRepository.findStudentDetailsByNameAndEmail(name,email); } }
 */